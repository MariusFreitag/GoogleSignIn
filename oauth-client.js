const { OAuth2Client } = require("google-auth-library");
const config = require("./config");

const minIat = {};

// Initialize min IAT to now to invalidate all old tokens
config().then((configObject) => {
  configObject.allowedUsers.forEach((user) => {
    if (!minIat[user]) {
      minIat[user] = Math.round(new Date().getTime() / 1000);
    }
  });
});

// Utility function to connect to the Google API
async function getPayload(clientId, token) {
  const client = new OAuth2Client(clientId);

  // Check if the token is valid
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: clientId,
  });

  // Return the payload of the token
  return ticket.getPayload();
}

module.exports = {
  verifyToken: async (clientId, token) => {
    try {
      const payload = await getPayload(clientId, token);

      // Abort if the payload is empty
      if (!payload) {
        throw new Error("Payload is empty");
      }

      // Abort if the user is not allowed
      if (!(await config()).allowedUsers.includes(payload.email)) {
        throw new Error("User is not allowed");
      }

      // Abort if the token is expired
      if (minIat[payload.email] && minIat[payload.email] > payload.iat) {
        throw new Error("Token expired");
      }

      // Store the token issue time as new minimum
      minIat[payload.email] = payload.iat;

      // Return a positive result
      console.log("Token verified successfully");
      return true;
    } catch (err) {
      // Return a negative result
      console.error("Error verifying token: " + err.message);
      return false;
    }
  },
  invalidateToken: async (clientId, token) => {
    try {
      const payload = await getPayload(clientId, token);

      // Abort if the payload is empty
      if (!payload) {
        throw new Error("Payload is empty");
      }

      // Abort if the user is not allowed
      if (!(await config()).allowedUsers.includes(payload.email)) {
        throw new Error("User is not allowed");
      }

      // Increase the minimum iat, if the token to invalidate was the latest
      minIat[payload.email] = Math.max(
        minIat[payload.email] || 0,
        payload.iat + 1
      );

      console.log("Token invalidated successfully");
    } catch (err) {
      console.error("Error invalidating token: " + err.message);
    }
  },
};
