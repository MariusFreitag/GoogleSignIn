FROM node:20

EXPOSE 3000

WORKDIR /app

# Install npm dependencies
COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install

# Copy project files
COPY . .

CMD ["npm", "start"]
