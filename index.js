const express = require("express");
const handlebars = require("express-handlebars");
const { verifyToken, invalidateToken } = require("./oauth-client");
const config = require("./config");
const app = express();

app.engine("handlebars", handlebars());
app.set("view engine", "handlebars");

app.use("/public", express.static("public"));
app.use("/robots.txt", express.static("public/robots.txt"));

app.get("/verify/:token", async (req, res) => {
  if (await verifyToken((await config()).googleClientId, req.params.token)) {
    res.send(true);
  } else {
    res.status(401);
    res.send(false);
  }
});

app.get("/invalidate/:token", async (req, res) => {
  await invalidateToken((await config()).googleClientId, req.params.token);
  res.send();
});

app.get("/", async function (req, res) {
  // Check if the redirection target is valid
  if (
    !req.query.redirect ||
    !(await config()).allowedHosts.includes(req.query.redirect)
  ) {
    req.query.redirect = null;
  }

  res.render("sso", {
    hasRedirect: !!req.query.redirect,
    redirect: `https://${req.query.redirect}`,
    hostname: req.hostname,
    clientId: (await config()).googleClientId,
  });
});

app.listen(3000, function () {
  console.log("GoogleSignIn connector started on http://localhost:3000");
});
