const { readFile, writeFile, stat } = require("fs/promises");

const configPath = "./persistence/config.json";
const defaultConfig = {
  allowedUsers: [],
  allowedHosts: [],
  googleClientId: "",
};

let config = undefined;

module.exports = async () => {
  if (config) {
    return config;
  }

  try {
    // Check if config file exists
    await stat(configPath);
  } catch {
    console.log("Writing default configuration");
    await writeFile(configPath, JSON.stringify(defaultConfig, null, 2));
  }

  config = JSON.parse(await readFile(configPath, "utf-8"));
  return config;
};
